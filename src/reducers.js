import isEqual from "lodash/fp/isEqual";
import PropTypes from "prop-types";

import { empty, firstProp, mapValues } from "./utils";

export const loadingValues = Object.freeze([
  "success",
  "reinit",
  "invalid",
  "init",
  "error",
]);

export const loadingNormalize = (loading) =>
  typeof loading === "string" && loadingValues.indexOf(loading) >= 0
    ? loading
    : undefined;

export const LoadingType = PropTypes.oneOf(loadingValues);

export const loadingUpdate = (oldLoading, newLoading) => {
  return oldLoading === newLoading ||
    newLoading === "exit" ||
    (oldLoading === "success" &&
      newLoading !== "init" &&
      newLoading !== "invalid" &&
      newLoading !== "error")
    ? undefined
    : newLoading === "init" &&
      (oldLoading === "invalid" || oldLoading === "success")
    ? "reinit"
    : newLoading === "error" &&
      (oldLoading === "invalid" || oldLoading === "success")
    ? "invalid"
    : newLoading;
};

const applyLoading = (state = empty.object, { loading, _expiry }) => {
  const update = loadingUpdate(state.loading, loading);
  switch (update) {
    case undefined:
      return state;
    case "init":
      return { loading: update, _expiry };
    case "reinit":
      return { ...state, loading: update, _expiry };
    default:
      return { ...state, loading: update };
  }
};

const applyLoadingWithId = (state = empty.object, { loading, id, _expiry }) => {
  const sub = state[id] || empty.object;
  const update = loadingUpdate(sub.loading, loading);
  switch (update) {
    case undefined:
      return state;
    case "init":
      return { ...state, [id]: { loading: update, _expiry } };
    case "reinit":
      return { ...state, [id]: { ...sub, loading: update, _expiry } };
  }

  const next = { ...sub, loading: update };
  if ((next.loading === "invalid" && !state[id]) || isEqual(next)(state[id])) {
    return state;
  }

  return { ...state, [id]: next };
};

const applyLoadingAll = (state = empty.object, loadingObj) =>
  mapValues((sub) => applyLoading(sub, loadingObj))(state);

export const loadingProgress = (loading) =>
  loading === "init" || loading === "reinit";

export const loadingPresent = (loading) =>
  loading === "success" || loading === "invalid" || loading === "reinit";

export const loadingValid = (loading) => loading === "success";

const dataProperty = (payload) => payload.data;

export default (
  prefix,
  { hasId = false, selector = dataProperty, ...customReducers } = empty.object
) => {
  prefix = `${prefix}_`;
  return (state = empty.object, action = empty.object) => {
    const { type, payload = empty.object } = action;
    const loading = {
      loading: payload.loading,
      _expiry: firstProp("_expiry", hasId && state[payload.id], state),
    };
    if (typeof customReducers[type] === "function") {
      return customReducers[type](state, action);
    }

    if (
      type.indexOf(prefix) !== 0 &&
      typeof customReducers[type] !== "string"
    ) {
      return state;
    }

    const subtype = customReducers[type] || type.substring(prefix.length);
    if (typeof customReducers[subtype] === "function") {
      return customReducers[subtype](state, action);
    }

    switch (subtype) {
      case "CLEAR":
        return empty.object;
      case "ERROR":
      case "EXIT":
      case "INIT":
        return hasId
          ? applyLoadingWithId(state, payload)
          : applyLoading(state, payload);
      case "INVALIDATE":
        return hasId
          ? applyLoadingWithId(state, {
              loading: "invalid",
              id: payload.id,
            })
          : applyLoading(state, { loading: "invalid" });
      case "INVALIDATEALL":
        return hasId
          ? applyLoadingAll(state, { loading: "invalid" })
          : applyLoading(state, { loading: "invalid" });
      case "SUCCESS":
        return hasId
          ? {
              ...state,
              [payload.id]: {
                ...selector(payload, state[payload.id]),
                ...loading,
              },
            }
          : { ...selector(payload), ...loading };
      case "HIT":
      case "QUIT":
        return state;
      default:
        throw new Error(`Unsupported action type ${type}.`);
    }
  };
};
