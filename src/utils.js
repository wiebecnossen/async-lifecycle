export const empty = Object.freeze({
  object: Object.freeze({}),
  array: Object.freeze([]),
  functionThatReturnsEmptyObject: () => empty.object,
  functionThatReturnsTrue: () => true,
});

export const mapValues = (mapper) => (obj) => {
  if (obj === undefined || obj === null) {
    return obj;
  }

  const result = {};
  for (const key in obj) {
    result[key] = mapper(obj[key], key);
  }

  return result;
};

export const firstProp = (name, ...objects) =>
  objects.reduce((r, obj) => r || (obj && obj[name]), undefined);
