import { empty } from "./utils";

export default (
  prefix,
  reducer,
  clearer = empty.functionThatReturnsEmptyObject
) => (state, action) => {
  const { type } = action || empty.object;
  return (type === `${prefix}_CLEAR` ? clearer : reducer)(state, action);
};
