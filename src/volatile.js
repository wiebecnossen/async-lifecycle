import map from "lodash/fp/map";
import some from "lodash/fp/some";
import PropTypes from "prop-types";
import { defaultMemoize } from "reselect";

import {
  LoadingType,
  loadingNormalize,
  loadingPresent,
  loadingProgress,
  loadingValid,
  loadingValues,
} from "./reducers";
import { empty } from "./utils";

export const VolatileShape = (innerPropType) =>
  PropTypes.shape({
    loading: LoadingType,
    present: PropTypes.bool.isRequired,
    progress: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired,
    value: innerPropType,
  });

export class Volatile {
  constructor(value, loading) {
    this.loading = loadingNormalize(loading);
    this.present = loadingPresent(loading);
    this.progress = loadingProgress(loading);
    this.valid = loadingValid(loading);
    this.value = value;
  }
}

export const intoVolatile = (property) =>
  defaultMemoize((value) => {
    if (property === undefined) {
      const { loading, ...obj } = value || empty.object;
      return new Volatile(obj, loading);
    } else {
      const { loading, [property]: obj } = value || empty.object;
      return new Volatile(obj, loading);
    }
  });

export const joinLoading = (left, right) => {
  if (left === "error" || right === "error") {
    return "error";
  }

  if (left === undefined || right === undefined) {
    return undefined;
  }

  if (loadingValues.indexOf(left) < loadingValues.indexOf(right)) {
    return joinLoading(right, left);
  }

  return left;
};

export const join = (select, ...values) =>
  new Volatile(
    some(({ present = false } = empty.object) => !present)(values)
      ? undefined
      : select(map("value")(values)),
    values.reduce(joinLoading, "success")
  );

export const volatile = (value, loading) => new Volatile(value, loading);

export const unwrap = ({ present, value }) => (present ? value : undefined);
