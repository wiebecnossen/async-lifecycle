import flow from "lodash/fp/flow";
import get from "lodash/fp/get";
import branch from "recompose/branch";
import mapProps from "recompose/mapProps";
import renderNothing from "recompose/renderNothing";

import { Volatile, joinLoading } from "./volatile";

const isPresentVolatile = (value) =>
  value instanceof Volatile ? value.present : value !== undefined;

const isValidVolatile = (value) =>
  value instanceof Volatile ? value.valid : value !== undefined;

const unwrapProp = (name) => ({
  [name]: value,
  loading = "success",
  ...props
}) => ({
  ...props,
  [name]: value instanceof Volatile ? value.value : value,
  loading: joinLoading(
    loading,
    value instanceof Volatile ? value.loading : "success"
  ),
});

const branchVolatile = (checkVolatile) => (propName, hoc = renderNothing) =>
  branch(
    flow(get(propName), checkVolatile),
    mapProps(unwrapProp(propName)),
    hoc
  );

export const branchPresent = branchVolatile(isPresentVolatile);

export const branchValid = branchVolatile(isValidVolatile);
