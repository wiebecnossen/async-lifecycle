// eslint-disable-next-line no-unused-vars
import regeneratorRuntime from "regenerator-runtime";

import { empty, firstProp } from "./utils";

export const defaultTimeout = 300000;

export const cancelResult = Object.freeze({ _cancel: true });

const { error: panic } = global["console"] || empty.object;

const asyncInvoke = async (method, ...args) => {
  if (typeof method === "function") {
    try {
      await method(...args);
    } catch (e) {
      panic("Panic: method in asyncInvoke threw an exception", e);
    }
  }
};

const withExpiry = (timeout) => (payload) => ({
  ...payload,
  _expiry: typeof timeout === "number" ? Date.now() + timeout : undefined,
});

const hasExpired = (expiry) =>
  typeof expiry === "number" && Date.now() >= expiry;

/**
 * Method to create an async actioncreator (Redux)
 * @param options.group
 * @param options.fire
 * @param options.args
 * @param options.key
 * @param options.cachekey
 * @param options.cacheValidation
 * @param options.container
 * @param options.callbackBefore
 * @param options.callbackSuccess
 * @param options.callbackAfter
 * @param options.callbackError
 * @param options.callbackQuit
 * @param options.extra
 * @param options.timeout
 */
const asyncActionCreator = ({
  group,
  fire,
  args = empty.array,
  key,
  cachekey,
  cacheValidation = empty.functionThatReturnsTrue,
  container = "data",
  callbackBefore,
  callbackAfter,
  callbackSuccess,
  callbackError,
  callbackQuit,
  extra,
  timeout = defaultTimeout,
}) => {
  const action = (type, payload) => ({
    type: `${group}_${type}`,
    payload,
    extra,
  });
  const id = key === "id" ? args[0] : undefined;

  return async (dispatch, getState) => {
    let containerState;
    if (
      cachekey &&
      (containerState =
        typeof container === "function"
          ? container(getState(), ...args)
          : getState()[container])
    ) {
      const cache = containerState[cachekey];
      const cached = cache && id ? cache[id] : cache;

      const loading = firstProp("loading", cached, cache, containerState);
      switch (loading) {
        case "success":
          if (
            hasExpired(firstProp("_expiry", cached, cache, containerState)) ||
            !cacheValidation(cached, getState)
          ) {
            break;
          }

          await asyncInvoke(callbackSuccess, cached, dispatch, args);
          dispatch(action("HIT", { id, loading: "success", data: cached }));
          return;

        case "init":
        case "reinit":
          dispatch(action("QUIT", { id, loading }));
          await asyncInvoke(callbackQuit, dispatch, args);
          return;
      }
    }

    dispatch(action("INIT", withExpiry(timeout)({ id, loading: "init" })));
    return await (async () => {
      await asyncInvoke(callbackBefore, dispatch, args);
      try {
        const payload = await fire.apply(this, args);
        await asyncInvoke(callbackSuccess, payload, dispatch, args);
        if (payload !== cancelResult) {
          dispatch(
            action("SUCCESS", { id, loading: "success", data: payload })
          );
          await asyncInvoke(callbackAfter, payload, dispatch, args);
        }
      } catch (e) {
        await asyncInvoke(callbackError, dispatch, args);
        dispatch(action("ERROR", { id, error: e, loading: "error" }));
      }

      dispatch(action("EXIT", { id, loading: "exit" }));
    })();
  };
};

export const composeAction = (...actions) => {
  const composeCallback = (f1, f2) => {
    const fun1 = typeof f1 === "function";
    const fun2 = typeof f2 === "function";
    return fun1 && fun2
      ? (...args) => Promise.all([f1(...args), f2(...args)])
      : fun1
      ? f1
      : fun2
      ? f2
      : undefined;
  };

  const composeProps = (a1, a2) => {
    const a1props = a1._properties || a1;
    const a2props = a2._properties || a2;
    return {
      ...a1props,
      ...a2props,
      callbackBefore: composeCallback(
        a1props.callbackBefore,
        a2props.callbackBefore
      ),
      callbackAfter: composeCallback(
        a1props.callbackAfter,
        a2props.callbackAfter
      ),
      callbackSuccess: composeCallback(
        a1props.callbackSuccess,
        a2props.callbackSuccess
      ),
      callbackError: composeCallback(
        a1props.callbackError,
        a2props.callbackError
      ),
      callbackQuit: composeCallback(a1props.callbackQuit, a2props.callbackQuit),
    };
  };

  const properties = actions.reduce(composeProps, empty.object);
  const action = asyncActionCreator(properties);
  action._properties = properties;
  return action;
};
