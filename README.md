# async-lifecycle

Generic async lifecycle actions and reducer.

## Methods

### `composeAction`

Creates an asynchronous action. The argument objects may have properties:

* `group` The name of the action group, required.
* `fire` The action function, required.
* `args = empty.array`
* `key`
* `cachekey`
* `cacheValidation` A predicate function that indicates whether cache content
  is valid. Default value always returns true.
* `container = "data"` - A `string` of the property for the container in the
  store or a `function` which returns the cache container `object`, like
  `(state, ...args) => state.foo.bar`
* `callbackBefore` Called before calling `fire`.
* `callbackSuccess` Called before dispatching the `SUCCESS` action.
* `callbackAfter` Called after dispatching the `SUCCESS` action.
* `callbackError` Called before dispatching the `ERROR` action.
* `extra`
* `timeout` Timeout until expiry in milliseconds, default 300000 (= 5 minutes).

When multiple objects are specified:
Callback functions are merged into wrapper functions, other properties are
overwritten as in `Object.assign`.

### `loadingProgress`

Determines if the given argument value corresponds to a state where data is
loading. E.g. `loadingProgress("init") // true`

### `loadingPresent`

Determines if the given argument value corresponds to a state where data is
present. E.g. `loadingPresent("init") // false`

### `asyncReducer`

Recalculates state from current state and an action.

## Action postfixes

| Value           | Description                  |
| --------------- | ---------------------------- |
| `INIT`          | Start                        |
| `HIT`           | Current data already present |
| `QUIT`          | Data already loading         |
| `SUCCESS`       | Data loaded successfully     |
| `ERROR`         | Error                        |
| `EXIT`          | Finish                       |
| `INVALIDATE`    | Mark data as not current     |
| `INVALIDATEALL` | Mark all data as not current |
| `CLEAR`         | Delete all data              |

## Values for `loading` property

| Value     | Description | Data present | Data current |
| --------- | ----------- | ------------ | ------------ |
| `init`    | Loading     | No           | N/A          |
| `success` | Success     | Yes          | Yes          |
| `invalid` | Invalidated | Yes          | No           |
| `reinit`  | Reloading   | Yes          | No           |
| `error`   | Error       | No           | N/A          |

## Build this package

```
yarn build
```

## Publish this package

```
npm login
cp package.json dist/
cd dist
npm publish
```
