import { mapValues } from "../src/utils";

describe("mapValues", () => {
  const data = { aap: 1 };
  it("maps correctly", () => {
    const actual = mapValues((value, key) => value + key.length)(data);
    expect(actual.aap).toEqual(4);
  });
});
