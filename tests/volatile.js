import { joinLoading } from "../src/volatile";

describe("volatile", () => {
  describe("joinLoading", () => {
    test("undefined+undefined", () =>
      expect(joinLoading(undefined, undefined)).toBeUndefined());
    test("undefined+init", () =>
      expect(joinLoading(undefined, "init")).toBeUndefined());
    test("undefined+success", () =>
      expect(joinLoading(undefined, "success")).toBeUndefined());
    test("undefined+invalid", () =>
      expect(joinLoading(undefined, "invalid")).toBeUndefined());
    test("undefined+reinit", () =>
      expect(joinLoading(undefined, "reinit")).toBeUndefined());
    test("undefined+error", () =>
      expect(joinLoading(undefined, "error")).toBe("error"));

    test("init+init", () => expect(joinLoading("init", "init")).toBe("init"));
    test("init+success", () =>
      expect(joinLoading("init", "success")).toBe("init"));
    test("init+invalid", () =>
      expect(joinLoading("init", "invalid")).toBe("init"));
    test("init+reinit", () =>
      expect(joinLoading("init", "reinit")).toBe("init"));
    test("init+error", () =>
      expect(joinLoading("init", "error")).toBe("error"));

    test("success+success", () =>
      expect(joinLoading("success", "success")).toBe("success"));
    test("success+invalid", () =>
      expect(joinLoading("success", "invalid")).toBe("invalid"));
    test("success+reinit", () =>
      expect(joinLoading("success", "reinit")).toBe("reinit"));
    test("success+error", () =>
      expect(joinLoading("success", "error")).toBe("error"));

    test("invalid+invalid", () =>
      expect(joinLoading("invalid", "invalid")).toBe("invalid"));
    test("invalid+reinit", () =>
      expect(joinLoading("invalid", "reinit")).toBe("invalid"));
    test("invalid+error", () =>
      expect(joinLoading("invalid", "error")).toBe("error"));

    test("reinit+reinit", () =>
      expect(joinLoading("reinit", "reinit")).toBe("reinit"));
    test("reinit+error", () =>
      expect(joinLoading("reinit", "error")).toBe("error"));

    test("error+error", () =>
      expect(joinLoading("error", "error")).toBe("error"));
  });
});
