import asyncReducer, {
  loadingPresent,
  loadingProgress,
  loadingUpdate,
} from "../src/reducers";

const success = Object.freeze({ loading: "success" });
const invalid = Object.freeze({ loading: "invalid" });
const asyncTest = () => {
  describe("invalidateall", () => {
    test("without id", () => {
      const reducer = asyncReducer("YO");
      const result = reducer(success, { type: "YO_INVALIDATEALL" });
      expect(result).toEqual(invalid);
    });
    test("with id", () => {
      const reducer = asyncReducer("YO", { hasId: true });
      const result = reducer(
        { aap: success, beer: success },
        { type: "YO_INVALIDATEALL" }
      );
      expect(result.aap).toEqual(invalid);
      expect(result.beer).toEqual(invalid);
    });
  });
};

describe("reducers", () => {
  describe("async", asyncTest);

  describe("loadingUpdate", () => {
    test("error overrides", () =>
      expect(loadingUpdate("init", "error")).toBe("error"));
    test("error invalidates", () =>
      expect(loadingUpdate("success", "error")).toBe("invalid"));
    test("init overrides", () =>
      expect(loadingUpdate(undefined, "init")).toBe("init"));
    test("init reinits", () =>
      expect(loadingUpdate("success", "init")).toBe("reinit"));
  });

  describe("loadingPresent", () => {
    describe("undefined", () => expect(loadingPresent()).toBe(false));
    describe("init", () => expect(loadingPresent("init")).toBe(false));
    describe("success", () => expect(loadingPresent("success")).toBe(true));
    describe("reinit", () => expect(loadingPresent("reinit")).toBe(true));
    describe("invalid", () => expect(loadingPresent("invalid")).toBe(true));
    describe("error", () => expect(loadingPresent("error")).toBe(false));
  });

  describe("loadingProgress", () => {
    describe("undefined", () => expect(loadingProgress()).toBe(false));
    describe("init", () => expect(loadingProgress("init")).toBe(true));
    describe("success", () => expect(loadingProgress("success")).toBe(false));
    describe("reinit", () => expect(loadingProgress("reinit")).toBe(true));
    describe("invalid", () => expect(loadingProgress("invalid")).toBe(false));
    describe("error", () => expect(loadingProgress("error")).toBe(false));
  });
});
